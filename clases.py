# Definición de la superclase Vehiculo
class Vehiculo:
    def __init__(self, marca, modelo, color): #constructor de la clase Vehiculo. Cuando creas una instancia de la clase Vehiculo, este método se llama automáticamente y asigna los valores pasados a los atributos marca, modelo y color de la instancia (self).
        self.marca = marca #asigna el valor del parámetro marca al atributo marca de la instancia.
        self.modelo = modelo #asigna el valor del parámetro modelo al atributo marca de la instancia.
        self.color = color #asigna el valor del parámetro color al atributo marca de la instancia.

    def mostrar_informacion(self):
        #f: cadena de texto dentro de una función
        print(f"Marca: {self.marca}\nModelo: {self.modelo}\nColor: {self.color}")

# Definición de la subclase Automovil que hereda de Vehiculo
class Automovil(Vehiculo):
    def __init__(self, marca, modelo, color, num_puertas): #Init: constructor de la clase
        # Llamamos al constructor de la superclase usando super()
        super().__init__(marca, modelo, color)
        self.num_puertas = num_puertas

    def mostrar_informacion(self):
        # Llamamos al método de la superclase usando super()
        super().mostrar_informacion()
        print(f"Número de puertas: {self.num_puertas}")

# Definición de la subclase Motocicleta que hereda de Vehiculo
class Motocicleta(Vehiculo):
    def __init__(self, marca, modelo, color, tipo):
        # Llamamos al constructor de la superclase usando super()
        super().__init__(marca, modelo, color)
        self.tipo = tipo

    def mostrar_informacion(self):
        # Llamamos al método de la superclase usando super()
        super().mostrar_informacion()
        print(f"Tipo: {self.tipo}")

# Solicitar al usuario que ingrese información para un automóvil
marca_auto = input("Ingrese la marca del automóvil: ")
modelo_auto = input("Ingrese el modelo del automóvil: ")
color_auto = input("Ingrese el color del automóvil: ")
puertas_auto = input("Ingrese el número de puertas del automóvil: ")

# Crear una instancia de la clase Automovil con la información proporcionada por el usuario
auto = Automovil(marca_auto, modelo_auto, color_auto, puertas_auto)

# Mostrar información del automóvil
print("\nInformación del Automóvil:")
auto.mostrar_informacion()
